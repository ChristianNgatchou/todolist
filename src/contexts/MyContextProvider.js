import React, { useState } from 'react';

import MyContext from './ContextApp';

function MyProvider(props){
    // Déclare une nouvelle variable d'état, qu’on va appeler « count »
    const [liste, setListe] = useState([]);

    return (

      <MyContext.Provider
        value={{
          liste,
          setTodo:todo=>{
            setListe([...liste, todo])
          },
          setDeleteListeChoseAFaire:tache=>{
            const NewListe = liste.filter((item) => item . text !== tache);
            setListe( NewListe );
          }
        }}
      >
        {props.children}
      </MyContext.Provider>
    );
    }

    export default MyProvider;