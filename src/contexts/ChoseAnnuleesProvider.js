import ChoseAnnuleesContext from './ChoseAnnuleesContext';
import React, { useState } from 'react';

function ChoseAnnuleesProvider(props){

    const [ChoseAnnulees, setChoseAnnulees] = useState([]);

    return (

      <ChoseAnnuleesContext.Provider
        value={{
          ChoseAnnulees,
          setAddDelete:tache=>{
            setChoseAnnulees([...ChoseAnnulees, tache])
          },
          setDeleteListeChoseAnnulees:tache=>{
            const NewListe = ChoseAnnulees.filter((item) => item . text !== tache);
            setChoseAnnulees( NewListe );
          }
        }}
      >
        {props.children}
      </ChoseAnnuleesContext.Provider>
    );
    }

    export default ChoseAnnuleesProvider;