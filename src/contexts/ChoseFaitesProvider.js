import ChoseFaitesContext from './ChoseFaitesContext';
import React, { useState } from 'react';

function ChoseFaitesProvider(props){

    const [ChoseFaites, setChoseFaites] = useState([]);

    return (

      <ChoseFaitesContext.Provider
        value={{
          ChoseFaites,
          setAddDo:tache=>{
            setChoseFaites([...ChoseFaites, tache])
          }
        }}
      >
        {props.children}
      </ChoseFaitesContext.Provider>
    );
    }

    export default ChoseFaitesProvider;