import React, { createContext } from 'react'

const ChoseFaitesContext = createContext()

export default ChoseFaitesContext;