import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';

import {Link} from 'react-router-dom'


const useNavStyles = makeStyles((theme) => ({
    myUL: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    root: {

    },
  }));

function Header() {
    const classes = useNavStyles();
  
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              <Link to="/">Add ToDoList</Link>
            </Typography>
            <Button color="inherit"><Link to="/chose-à-faire">Taches à faire</Link></Button>
            <Button color="inherit"><Link to="/chose-faites">Taches Accomplies</Link></Button>
            <Button color="inherit"><Link to="/chose-Annulees">Taches Annulées</Link></Button>
            <Button color="inherit"><Link to="/propos">A Propos</Link></Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }

  export default Header;