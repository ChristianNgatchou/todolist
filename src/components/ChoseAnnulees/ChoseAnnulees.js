import React, { useContext, useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import ChoseAnnuleesContext from '../../contexts/ChoseAnnuleesContext'
import ChoseAFaire from '../../contexts/ContextApp'

function ChoseAnnulees(){
    let count=0;
    const { ChoseAnnulees, setDeleteListeChoseAnnulees } = useContext(ChoseAnnuleesContext);
    const { setTodo, liste } = useContext(ChoseAFaire);
  
    function isEmpty(obj) {
      for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
          return false;
      }
      return true;
    }
    
    if(isEmpty(ChoseAnnulees)){
      return(
        <div>
          <br/>
          <center>
            <h3> Liste des taches Annulées </h3>
            <p className="listevide"><br/>Aucune tache n'a été Annulée</p>
          </center>
        </div>
      );
    }else{
    return (
        <div id="myUL">
          <br/>
          <center>
            <h3>Liste des taches Annulées </h3>
          </center>
          <Grid container spacing={12}>
            <Grid item xs={12} md={12}>
                <List>
                  {
                    ChoseAnnulees.map((elemnt) => (
                      <ListItem key={count++}>
                      <ListItemText
                        primary={elemnt.text}
                      />
                      <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="add" color="primary" onClick={()=>{
                            setTodo(elemnt)
                            setDeleteListeChoseAnnulees(elemnt.text)
                            }}>
                          <AddIcon />
                        </IconButton>
                      </ListItemSecondaryAction>
                      </ListItem>
                    ))
                  }
                </List>
            </Grid>
          </Grid>
        </div>
    );
  }
}

export default ChoseAnnulees;