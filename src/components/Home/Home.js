import React, { useState, useContext } from 'react';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import MyContext from '../../contexts/ContextApp'
import '../../index.css';
import '../../App.css';
 
function Home(){
  const { setTodo, liste} = useContext(MyContext);
  const [text, setText] = useState("");

  const Addtext = (e) => {
    setText({text: e.target.value});
  }

  return(
    <div>
      <form> 
        <center>
          <br/>
          <h3>Formulaire d'ajout de Nouvelles Taches</h3>
        </center>
        <div className="container">
          <label>
            Ajouter Une Tache :
            <input type="text" placeholder="Entrer une Tache A faire" name="name" id="name" onChange={Addtext} required/>
          </label>
          <input type="button" className="registerbtn" value="Ajouter" onClick={()=>setTodo(text)}/>
        </div>
        <br/>
      </form>
    </div>
  )
}

export default Home;