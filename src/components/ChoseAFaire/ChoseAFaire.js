import React, { useContext } from 'react';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';


import ContextApp from '../../contexts/ContextApp'
import ChoseAnnuleesContext from '../../contexts/ChoseAnnuleesContext'
import ChoseFaitesContext from '../../contexts/ChoseFaitesContext'

function ChoseAFaire(){
    let count=0;
    const { liste, setDeleteListeChoseAFaire } = useContext(ContextApp);
    const { ChoseAnnulees, setAddDelete } = useContext(ChoseAnnuleesContext);
    const { ChoseFaites, setAddDo } = useContext(ChoseFaitesContext);

    function isEmpty(obj) {
      for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
          return false;
      }
      return true;
  }
  
    if(isEmpty(liste)){
      return(
        <div>
          <br/>
          <center>
            <h3> Liste des taches A faire </h3>
            <p className="listevide"><br/>Aucune tache n'a été renseignée</p>
          </center>
        </div>
      );
    }
    else{
      return (
        <div id="myUL">
          <br/>
          <Grid container spacing={12}>
            <Grid item xs={12} md={12}>
              <div>
                <center>
                  <h3>Liste des taches A faire </h3>
                </center>
                <List>
                  {
                    liste.map((elemnt) => (
                      <ListItem key={count++}>
                      <ListItemText
                        primary={elemnt.text}
                      />
                      <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="add" color="primary" onClick={()=>{
                          setAddDo(elemnt)
                          setDeleteListeChoseAFaire(elemnt.text)}}>
                          <AddIcon />
                        </IconButton>
                          <IconButton edge="end" aria-label="delete" color="secondary" onClick={()=>{
                            setAddDelete(elemnt)
                            setDeleteListeChoseAFaire(elemnt.text)
                            }}>
                            <DeleteIcon />
                          </IconButton>
                      </ListItemSecondaryAction>
                      </ListItem>
                    ))
                  }
                </List>
              </div>
            </Grid>
          </Grid>
        </div>
      );
    }
}

export default ChoseAFaire;