import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import MyProvider from './contexts/MyContextProvider';
import ChoseAnnuleesProvider from './contexts/ChoseAnnuleesProvider';
import ChoseFaitesProvider from './contexts/ChoseFaitesProvider';
import './App.css';
import Header from './components/Header/Header';
import Propos from './components/Propos/Propos';
import ChoseAFaire from './components/ChoseAFaire/ChoseAFaire';
import ChoseAnnulees from './components/ChoseAnnulees/ChoseAnnulees';
import ChoseFaites from './components/ChoseFaites/ChoseFaites';
import Home from './components/Home/Home';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Header />
        <MyProvider>
          <Route path="/" exact component={Home} />
          <ChoseAnnuleesProvider>
          <ChoseFaitesProvider>
            <Route path="/chose-à-faire" exact component={ChoseAFaire} />
            <Route path="/chose-Annulees" exact component={ChoseAnnulees} />
              <Route path="/chose-faites" exact component={ChoseFaites} />
            </ChoseFaitesProvider>
          </ChoseAnnuleesProvider>
        </MyProvider>
          <Route path="/propos" exact component={Propos} />
      </Router>
    );
  }
}

export default App;
